import os
import mysql.connector
from mysql.connector import errorcode
import yaml
import query as lumini_query
from datetime import datetime
import pickle


def connect_mysql(db_access):
    try:
        cnx = mysql.connector.connect(user=db_access['username'], password=db_access['password'],
                                      host=db_access['host'], port=db_access['port'],
                                      database=db_access['database'])
    except mysql.connector.Error as err:
        if err.errno == errorcode.ER_ACCESS_DENIED_ERROR:
            print("Something is wrong with your user name or password")
        elif err.errno == errorcode.ER_BAD_DB_ERROR:
            print("Database does not exist")
        else:
            print(err)
    return cnx


def close_mysql():
    global cnx
    if cnx is not None:
        cnx.cursor().close()
        cnx.close()
        cnx = None


config = yaml.safe_load(open("lumini_config.yml"))
access = dict()
access['host'] = config['access']['host']
access['port'] = config['access']['port']
access['database'] = config['access']['database']
access['username'] = config['access']['username']
access['password'] = config['access']['password']

cnx = connect_mysql(access)
cursor = cnx.cursor()

MALE = 0
FEMALE = 1
query = lumini_query.lumini_query(
    age_range=[datetime(1991, 1, 1), datetime(1991, 12, 31)],
    gender=MALE
)
cursor.execute(query)
suffix_list = ['polarized', 'uv', 'white']

bucket = "lumini-b2b-origin"
filename = '.'.join([bucket+"_map", "bin"])
map_file = open(filename, 'rb')
path_map = pickle.load(map_file)

for idx,  data in enumerate(cursor):
    uid, phone, name, birth, gender, created_at = data
    file_prefix = str(created_at).replace('-', '').replace(':', '').replace(' ', '_')
    if file_prefix in path_map:
        path = path_map[file_prefix]
        target_files = []
        for filename in os.listdir(path):
            if file_prefix in filename:
                target_files.append(filename)
        print(target_files)



close_mysql()
# TODO
# 1. kiosk, mirror -> 성별, 나이 query
# 2. skin data (score) uid, uuid db 구조 파악
# 3. 성별 0, 1 잘못 구별되어 있을 수 있음
# 4. lumini-b2b-origin / lululab-kwmirror
