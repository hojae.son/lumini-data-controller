import os
import pickle
path_map = dict()


def create_path_map(root):
    global path_map
    for root, dirs, files in os.walk("./" + root):
        for file in files:
            key = "_".join(file.split("_")[:2])
            value = root
            path_map[key] = value


bucket = "lumini-b2b-origin"
create_path_map(bucket)
filename = '.'.join([bucket+"_map", "bin"])
file = open(filename, "wb")

pickle.dump(path_map, file)
file.close()

map_file = open(filename, 'rb')
path_map = pickle.load(map_file)
print(path_map, type(path_map))