#
# sample_query = "select u2.id, c2.phone , u2.name, u2.birthday, s.`datetime` from skindata s "\
#                "inner join `user` u2 on u2.id = s.user_id inner join corpuser c2 on c2.id = s.corpuser_id "\
#                "where c2.phone ='gmrc11' and s.`datetime` >= '2021-03-08' and s.`datetime` <'2021-03-10' group by u2.id "

def lumini_query(age_range=None, gender=None):
    base_query = "select u2.id, c2.phone , u2.name, u2.birthday, u2.gender ,s.`datetime` from skindata s " \
            "inner join `user` u2 on u2.id = s.user_id inner join corpuser c2 on c2.id = s.corpuser_id "
    age_from, age_to = age_range[0], age_range[1]
    formatted_age_from = '-'.join(list(map(str, [age_from.year, age_from.month, age_from.day])))
    formatted_age_to = '-'.join(list(map(str, [age_to.year, age_to.month, age_to.day])))

    age_condition = "'" + formatted_age_from + "'" + " <= u2.`birthday`  and u2.`birthday` <= '" + formatted_age_to + "'"
    gender_condition = "u2.`gender` = '" + str(gender) + "'"

    condition = "where " + gender_condition + " and " + age_condition
    query = base_query + condition
    return query
