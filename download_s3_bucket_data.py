import os
import subprocess


def execute_s3_command(*args):
    print(list(args))
    base = ["aws", "s3"]
    base.extend(list(args))
    print(base)
    proc = subprocess.Popen(base, stdout=subprocess.PIPE, universal_newlines=True, shell=True)
    out, err = proc.communicate()
    return out


def get_s3_bucket_list():
    out = execute_s3_command('ls')
    buckets = []
    for idx, line in enumerate(out.split()):
        if idx % 3 == 2:
            buckets.append(line)
    return buckets


bucketList = get_s3_bucket_list()

# cmd = "aws s3 cp --recursive --exclude * --include '*.png' s3://lumini-b2b-origin/" + " ."
# print(execute_s3_command(cmd.split()))


import boto3

s3 = boto3.client('s3')  # again assumes boto.cfg setup, assume AWS S3

unableToDownloadList = open('unabletoDownload.txt', 'w')
for bucket in bucketList:
    paginator = s3.get_paginator('list_objects_v2')
    response_iterator = paginator.paginate(
        Bucket=bucket,
    )
    try:
        for page in response_iterator:
            for key in page['Contents']:
                path, filename = '/'.join(key['Key'].split('/')[:-1]), key['Key'].split('/')[-1]
                path = path.replace(':', '=')
                os.makedirs('/'.join([bucket, path]), exist_ok=True)
                try:
                    path_to_store = '/'.join(['/'.join([bucket, path]), filename])
                    if not os.path.isfile(path_to_store):
                        print('Download following file ' + path_to_store)
                        s3.download_file(bucket, key['Key'], path_to_store)

                except:
                    print('unable ' + path_to_store)
                    unableToDownloadList.write('path error' + path_to_store + '\n')
    except:
        unableToDownloadList.write('bucket key error ' + str(bucket))

unableToDownloadList.close()
